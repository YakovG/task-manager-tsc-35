package ru.goloshchapov.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;

public class TaskRepositoryTest {

    private User user;

    private Project project;

    @Before
    public void before() {
        user = new User();
        project = new Project();
    }

    @Test
    public void testCreate() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        final Task taskById = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
    }

    @Test
    public void testRemove() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        final Task taskById = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        Assert.assertNotNull(taskRepository.removeOneById(taskById.getId()));
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void testClear() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        final Task task1 = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertNotNull(taskRepository.add(task1));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void testSize() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        final Task task1 = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertNotNull(taskRepository.add(task1));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(2,taskRepository.size());
    }

    @Test
    public void testFindAllByProjectId() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("PROJECT_ID");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("PROJECT_ID", task.getProjectId());
        task.setUserId(user.getId());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
    }

    @Test
    public void testRemoveAllByProjectId() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("PROJECT_ID");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("PROJECT_ID", task.getProjectId());
        task.setUserId(user.getId());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        Assert.assertFalse(taskRepository.removeAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
    }

    @Test
    public void testBindUnbind() {
        final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertNotNull(task.getUserId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        Assert.assertNotNull(taskRepository.bindToProjectById(user.getId(), task.getId(), project.getId()));
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        Assert.assertNotNull(taskRepository.unbindFromProjectById(user.getId(), task.getId()));
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
    }
}
