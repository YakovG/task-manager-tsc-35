package ru.goloshchapov.tm.exception.entity;

public class UserNoProjectsException extends ArithmeticException{

    public UserNoProjectsException() {
        super ("Error! User has no active projects...");
    }

}
