package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull public static final String NAME = "task-list";

    @NotNull public static final String DESCRIPTION = "Show task list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> tasks = endpointLocator.getTaskEndpoint().findTaskAllByUserId(session);
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task.getId() + " : " + task.getName());
            index++;
        }
    }
}
