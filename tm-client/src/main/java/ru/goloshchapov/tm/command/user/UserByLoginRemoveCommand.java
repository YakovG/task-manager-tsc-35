package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.User;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByLoginRemoveCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-remove-by-login";

    @NotNull public static final String DESCRIPTION = "Remove user by login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final User user = endpointLocator.getAdminEndpoint().removeUserByLogin(session, login);
        final boolean result = endpointLocator.getTaskProjectEndpoint().removeAllTaskByUserId(session);
        if (!result) System.out.println("User by login " + login + "has had an EMPTY project list");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
